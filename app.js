const form = document.querySelector("form");

function accordion() {
    let acc = document.getElementsByClassName("accordion");
    for (let i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            let panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
}



function getAllSessionKeys() {
    return Object.keys(sessionStorage);
}


function getAllSessionNames() {
    allSessionKeys.forEach(function (key) {
        let pizzas = sessionStorage.getItem(key)
        let pizzasParsed = JSON.parse(pizzas)
        console.log(pizzasParsed.name);
    })
}

function isToppingsValid(toppings) {
    return toppings.length > 1 ? true : false;
}

function isUnique(element) {
    err = 0;

    let errorElement = document.querySelector(".error");

    for (let i = 0; i < sessionStorage.length; i++) {
        if (JSON.parse(Object.values(sessionStorage)[i]).name == element.value) {
            err++
        } else {
            errorElement.style.display = "none";
        }
    }
    if (err > 0) {
        errorElement.style.display = "inline";
        errorElement.innerHTML = "This name is already taken!";
    }
}

function checkUniqueName() {
    let name = document.querySelector(".isUnique");
    name.addEventListener("change", (event) => {
        isUnique(event.target);
    });
}

function removeItem(element) {
    let r = confirm("You're going to delete this pizza! You sure?");
    if (r == true) {
        deleteDivs()
        sessionStorage.removeItem(element)
        createDivs()
    } else {
    }
}

function saveForm(formData) {
    const checkedPizzas = formData.getAll("pizza")[0];
    console.log(checkedPizzas);
    let toppings = formData.getAll("topping");

    formData.set("toppings", toppings)

    const formObj = Object.fromEntries(formData.entries());
    sessionStorage.setItem(formObj.name, JSON.stringify(formObj))

    location.reload();
}

function initPizzasForm() {
    checkUniqueName();


    form.addEventListener("submit", (e) => {
        // while (document.querySelector(".error_name") == true) {
        //     checkUniqueName();
        // }

        e.preventDefault();

        const formData = new FormData(e.target);
        let toppings = formData.getAll("topping");

        if (isToppingsValid(toppings)) {

            saveForm(formData);

        } else {
            let errorElement = document.querySelectorAll(".error")[1];
            errorElement.style.display = "inline";
            errorElement.innerHTML = "Must be checked at least 2 toppings!";
        }
    })
}

function deleteDivs() {
    let pizzaElements = document.querySelectorAll(".pizzaElement");
    if (pizzaElements.length > 0) {
        for (let i = 0; i < sessionStorage.length; i++) {
            pizzaElements[i].remove()
        }
    } else {
    }
}

function checkCreatedPizzas() {
    if (sessionStorage.length > 0) {
        createDivs();
    } else {
        document.querySelector("div.allPizza").innerHTML = "<b><center>Unfortunately, no pizzas yet created...<center></b>";
    }
}

function createDivs() {

    let currentDiv = document.querySelector("div.allPizza");
    let output = "";

    for (let i = 0; i < sessionStorage.length; i++) {

        let objValues = Object.values(sessionStorage)[i]
        let obj = JSON.parse(objValues)

        let chilli = `<img class="chilli" src="./chilli.png">`
        let numChilli = chilli.repeat(obj.heat)

        if (typeof (obj.pizza) == "undefined") {
            obj.pizza = `./emptypizza.png`
        }

        output +=
            `<div class="pizzaElement">
            <div class="elementImg">
            <img src=${obj.pizza}></img>
            </div>
            <div class="elementInfo">
            <h2> ${obj.name}</h2 >
            <p>${obj.toppings}</p>
            ${numChilli}<br>
        <h3>${obj.price}€</h3>
        </div>
        <button class="btnDelete" onclick="removeItem('${obj.name}')">x</button></div>
        `
    }
    currentDiv.innerHTML = output;
}

(function () {
    accordion()
    initPizzasForm();
    checkCreatedPizzas();
})();